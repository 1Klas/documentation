#include "Lab04.h"

#include <qmessagebox.h>
#include <qjsonobject.h>
#include <qjsondocument.h>
#include <qnetworkrequest.h>
#include <qnetworkreply.h>
#include <qscrollbar.h>

enum class Provider
{
	OpenAI,
	Anthropic
};

enum class OpenAiModel
{
	GPT_3_5_TURBO,
	GPT_3_5_TURBO_16K,
	GPT_4_TURBO,
};

enum class AnthropicModel
{
	Opus,
	Sonnet,
	Haiku
};

Lab04::Lab04(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
	PopulateOpenAiModels();
	connect(&netManager, &QNetworkAccessManager::finished, this, &Lab04::HandleNetworkReply);
	connect(ui.providerComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Lab04::on_providerComboBox_currentIndexChanged);
	connect(ui.sendButton, &QPushButton::clicked, this, &Lab04::on_sendButton_clicked);
}

Lab04::~Lab04()
{}

void Lab04::PopulateAnthropicModels()
{
    ui.modelComboBox->clear();
	ui.modelComboBox->addItem("Claude 3 Opus");
	ui.modelComboBox->addItem("Claude 3 Sonnet");
	ui.modelComboBox->addItem("Claude 3 Haiku");
}

void Lab04::PopulateOpenAiModels()
{
    ui.modelComboBox->clear();
	ui.modelComboBox->addItem("GPT-3.5-turbo");
	ui.modelComboBox->addItem("GPT-3.5-turbo-16k");
	ui.modelComboBox->addItem("GPT-4-turbo");
}

bool Lab04::Message(QString message)
{
	QString apiKey = ui.apiKeyLineEdit->text();
	QString modelString;

	Provider provider = static_cast<Provider>(ui.providerComboBox->currentIndex());
	if (provider == Provider::OpenAI)
	{
		OpenAiModel model = static_cast<OpenAiModel>(ui.modelComboBox->currentIndex());

		if (model == OpenAiModel::GPT_3_5_TURBO) modelString = "gpt-3.5-turbo";
		else if (model == OpenAiModel::GPT_3_5_TURBO_16K) modelString = "gpt-3.5-turbo-16k";
		else if (model == OpenAiModel::GPT_4_TURBO) modelString = "gpt-4-turbo";
		else
		{
			QMessageBox::critical(this, "Error", "Please select a model.");
			return false;
		}

		QUrl url("https://api.openai.com/v1/chat/completions");
		QNetworkRequest request(url);
		request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
		request.setRawHeader("Authorization", ("Bearer " + apiKey).toUtf8());

		QJsonObject json;
		json.insert("model", modelString);
		json.insert("max_tokens", 512);
		AppendMessageToChatHistory(message, ChatRole::User);
		json.insert("messages", ConvertChatHistoryToJson());
		QJsonDocument doc(json);
		netManager.post(request, doc.toJson(QJsonDocument::Compact));
	}
	else
	{
		AnthropicModel model = static_cast<AnthropicModel>(ui.modelComboBox->currentIndex());

		if (model == AnthropicModel::Opus) modelString = "claude-3-opus-20240229";
		else if (model == AnthropicModel::Sonnet) modelString = "claude-3-sonnet-20240229";
		else if (model == AnthropicModel::Haiku) modelString = "claude-3-haiku-20240307";
		else
		{
			QMessageBox::critical(this, "Error", "Please select a model.");
			return false;
		}

		QUrl url("https://api.anthropic.com/v1/messages");
		QNetworkRequest request(url);
		request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
		request.setRawHeader("x-api-key", apiKey.toUtf8());
		request.setRawHeader("anthropic-version", "2023-06-01");

		QJsonObject json;
		json.insert("model", modelString);
		json.insert("max_tokens", 512);
		AppendMessageToChatHistory(message, ChatRole::User);
		json.insert("messages", ConvertChatHistoryToJson());
		QJsonDocument doc(json);
		netManager.post(request, doc.toJson());
	}

	isRequesting = true;

	return true;
}

void Lab04::AppendMessageToChatHistory(QString message, ChatRole role, QColor color)
{
	chatHistory.push_back({ message, role });

	ui.chatHistoryBrowser->clear();
	for (int i = 0; i < chatHistory.size(); i++)
	{
		ChatEntry entry = chatHistory[i];
		if (i == chatHistory.size() - 1)
		{
			ui.chatHistoryBrowser->setTextColor(color);
		}
		ui.chatHistoryBrowser->append(entry.role == ChatRole::User ? "------------You------------\n" : "---------Assistant---------\n");
		ui.chatHistoryBrowser->append(entry.content);
		ui.chatHistoryBrowser->append("\n");

		ui.chatHistoryBrowser->moveCursor(QTextCursor::End);
		ui.chatHistoryBrowser->setTextColor(Qt::white);

		QScrollBar* scrollbar = ui.chatHistoryBrowser->verticalScrollBar();
		scrollbar->setValue(scrollbar->maximum());
	}
}

void Lab04::HandleNetworkReply(QNetworkReply* reply)
{
	isRequesting = false;
	if (reply->error() == QNetworkReply::NoError)
	{
		QByteArray data = reply->readAll();
		QJsonDocument doc = QJsonDocument::fromJson(data);
		QJsonObject obj = doc.object();

		QString content;
		if (QJsonArray arr = obj["content"].toArray(); !arr.isEmpty())
		{
			QJsonObject first = arr[0].toObject();
			if (first.contains("text"))
			{
				content = first["text"].toString();
			}
			else
			{
				content = "--- Error: Content is not text ---";
			}
		}
		else
		{
			content = "--- Error: No content ---";
		}

		AppendMessageToChatHistory(content, ChatRole::Assistant);
	}
	else
	{
		auto data = reply->readAll();
		AppendMessageToChatHistory(QString(data), ChatRole::Assistant, Qt::red);
		shouldDeleteNLastMessages = 2;
		ui.inputTextEdit->setPlainText(lastMessage);
	}
}

QJsonArray Lab04::ConvertChatHistoryToJson() const
{
	Provider provider = static_cast<Provider>(ui.providerComboBox->currentIndex());
	QJsonArray arr;
	for (const auto& entry : chatHistory)
	{
		QJsonObject obj;
		obj.insert("role", (entry.role == ChatRole::User ? "user" : provider == Provider::Anthropic ? "assistant" : "system"));
		obj.insert("content", entry.content);
		arr.append(obj);
	}
	return arr;
}

void Lab04::on_sendButton_clicked()
{
	lastMessage = ui.inputTextEdit->toPlainText();
	if (lastMessage.isEmpty()) return;

	if (isRequesting)
	{
		QMessageBox::critical(this, "Error", "Please wait for the previous request to finish");
		return;
	}

	if (ui.apiKeyLineEdit->text().isEmpty())
	{
		QMessageBox::critical(this, "Error", "Invalid API key");
		return;
	}

	if (shouldDeleteNLastMessages > 0)
	{
		chatHistory.erase(chatHistory.end() - shouldDeleteNLastMessages, chatHistory.end());
		shouldDeleteNLastMessages = 0;
	}

	if (!Message(lastMessage)) return;
	ui.inputTextEdit->clear();
}

void Lab04::on_providerComboBox_currentIndexChanged(int index)
{
	Provider provider = static_cast<Provider>(index);
	if (provider == Provider::OpenAI)
	{
		PopulateOpenAiModels();
	}
	else
	{
		PopulateAnthropicModels();
	}
}
