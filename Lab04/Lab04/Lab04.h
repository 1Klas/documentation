#pragma once

#include <QtWidgets/QMainWindow>
#include <QtNetwork/QNetworkAccessManager>
#include "ui_Lab04.h"
#include <qjsonarray.h>

enum class ChatRole
{
	User,
	Assistant
};

struct ChatEntry
{
	QString content;
	ChatRole role;
};

class Lab04 : public QMainWindow
{
    Q_OBJECT

public:
    Lab04(QWidget *parent = nullptr);
    ~Lab04();

    bool Message(QString message);

    void PopulateAnthropicModels();
    void PopulateOpenAiModels();
    void AppendMessageToChatHistory(QString message, ChatRole role, QColor color = Qt::white);

    void HandleNetworkReply(QNetworkReply* reply);
    QJsonArray ConvertChatHistoryToJson() const;

private slots:
    void on_sendButton_clicked();
	void on_providerComboBox_currentIndexChanged(int index);

private:
    Ui::Lab04Class ui;
    QNetworkAccessManager netManager = QNetworkAccessManager(this);
    QList<ChatEntry> chatHistory;
    QString lastMessage = "";
    int shouldDeleteNLastMessages = 0;
    bool isRequesting = false;
};
